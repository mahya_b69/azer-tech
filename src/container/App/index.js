import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import ResponsiveDrawer from "../components/ResponsiveDrawer";
import Home from "../components/Home";
import WhoWeAre from "../components/WhoWeAre";
import MobileApplications from "../components/MobileApplications";
import Partners from "../components/Partners";
import Careers from "../components/Careers";
import Contact from "../components/Contact";
import Footer from "../components/Footer";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles(theme => ({
    canvas: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        overflowX: 'hidden',
        backgroundColor: '#fff',
        backgroundRepeat: 'repeat'
    }
    ,
    canvasContainer: {
        minHeight: '100%',
        paddingTop: '0px',
        position: 'relative',
        overflowX: 'hidden!important',
        width: '100vw'
    }

}))
export default function App() {
    const classes = useStyles();
    return (
        <div className={classes.canvas}>
            <div className={classes.canvasContainer}>
                <div id="wsb-canvas-template-page" className="wsb-canvas-page page">
                    <div id="wsb-canvas-template-container">
                        <Router>
                            <ResponsiveDrawer/>
                            <Switch>
                                <Route exact path={"/"}>
                                    <Home/>
                                </Route>
                                <Route exact path={"/home"}>
                                    <Home/>
                                </Route>
                                <Route exact path={"/who-we-are"}>
                                    <WhoWeAre/>
                                </Route>
                                <Route exact path={"/mobile-applications"}>
                                    <MobileApplications/>
                                </Route>
                                <Route exact path={"/partners"}>
                                    <Partners/>
                                </Route>
                                <Route exact path={"/careers"}>
                                    <Careers/>
                                </Route>
                                <Route exact path={"/contact-us"}>
                                    <Contact/>
                                </Route>
                            </Switch>
                        </Router>
                    </div>
                </div>

            </div>
        </div>


    )
}