import React from "react";
import parallax from "../../assets/images/parallax.jpg";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {BrowserRouter as Link} from "react-router-dom";
import whoWeAre from "../../assets/images/who-we-are.png";
import whatWeCanDo from "../../assets/images/what-we-can-do.png";
import callUs from "../../assets/images/call-us.png";
import learnMore from "../../assets/images/learn-more.jpg";
import Button from "@material-ui/core/Button";
import call from "../../assets/images/call.jpg";
import copyright from "../../assets/images/copyright.png";

const useStyles = makeStyles(theme => ({
    content: {
        flexGrow: 1,
        padding: '0',
    },
    parallax: {
        width: '100%',
        height: '560px',
        backgroundAttachment: 'fixed',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        zIndex: '10px'
    },
    container: {
        width: '1000px',
        height: '100vh',
        padding: '0',
        margin: '0 auto',
        position: 'relative',
        backgroundColor: 'transparent'

    },
    shape1: {
        position: 'absolute',
        top: '420px',
        left: '65px',
        zIndex: '102px',

    },
    shape2: {
        position: 'absolute',
        top: '420px',
        left: '383px',
        zIndex: '102px',

    },
    shape3: {
        position: 'absolute',
        top: '420px',
        left: '701px',
        zIndex: '102px',

    },
    shapeArrow: {
        boxSizing: 'content-box',
        width: '216px !important',
        height: '179px !important',
        display: 'inline-block',
        position: 'relative',
        border: '8px solid white',

    },
    triangleDown: {
        position: 'absolute',
        bottom: '-26px',
        zIndex: '10000px',
        width: '0',
        height: '0',
        borderLeft: '108px solid transparent',
        borderRight: '108px solid transparent',
    },
    img: {
        width: '64px',
        height: '64px',
        position: 'relative',
        overflow: 'hidden',
    },
    image: {
        verticalAlign: 'middle',
        width: '64px',
        height: '64px',
        position: 'absolute',

    },
    text: {
        position: 'absolute',
        top: '437px',
        left: '82px',
        zIndex: '86'
    },
    txt: {
        width: '195px',
        height: '78px'
    },
    learn: {
        position: 'absolute',
        top: '800px',
        left: '-8px',
        zIndex: '98',
        border: '5px solid #fff'


    },
    learnMoreBtn: {
        width: '149px',
        height: '56px',
        backgroundColor: 'rgba(255,255,255,.3)',
        border: '3px solid #fff',
        position: 'relative',
        '&:hover': {
            backgroundColor: '#426174'
        }
    },
    footer: {
        position: 'relative',
        height: '200px',
        width: '100vw',
        margin: 'auto',
        color: '#fff',
        backgroundColor: '#3e3e45',
        fontSize: '15px',
        fontFamily: 'Lato-Regular',
        zIndex: '200',
        bottom: '0',
        left: '0'


    },
    footerContainer: {
        position: 'absolute',
        width: '1000px',
        left: '50%',
        marginLeft: '-500px'

    },
    footerText: {
        top: '32px',
        left: '166px',
        position: 'absolute',
        zIndex: '14px'
    },
    footerImage: {
        top: '20px',
        left: '15px',
        position: 'absolute',
        zIndex: '11',
        color: '#fff'
    },
    footerImg: {
        width: '133px',
        height: '35px',
        position: 'relative',
        overflow: 'hidden'
    },
    copyright: {
        verticalAlign: 'middle',
        width: '133px',
        height: '35px'
    }


}))


export default function Home() {
    const classes = useStyles();

    return <div style={{height: '100%'}} className={classes.content}>

        <div
            className={classes.parallax}
            style={{
                transform: 'transform',
                backgroundImage: "url(" + parallax + ")",
            }}>
            <div className={classes.container}>
                <div style={{position: 'absolute', top: '180px', left: '8px', zIndex: '8px', width: '100%'}}>
                    <h1 style={{textAlign: 'center'}}>
                        <span style={{color: '#000', margin: '0 auto', fontFamily: 'Lato-light'}}>
                        Empowering your mobile workforce
                    </span>
                    </h1>
                </div>
                <div className={classes.shape1}>
                    <div className={classes.shapeArrow} style={{backgroundColor: '#a1a1a1'}}>
                        <div className={classes.triangleDown} style={{borderTop: '26px solid #a1a1a1'}}></div>
                    </div>
                </div>
                <div className={classes.shape2}>
                    <div className={classes.shapeArrow} style={{backgroundColor: '#dd492f'}}>
                        <div className={classes.triangleDown} style={{borderTop: '26px solid #dd492f'}}></div>
                    </div>
                </div>
                <div className={classes.shape3}>
                    <div className={classes.shapeArrow} style={{backgroundColor: '#a1a1a1'}}>
                        <div className={classes.triangleDown} style={{borderTop: '26px solid #a1a1a1'}}></div>
                    </div>
                </div>
                <div style={{position: 'absolute', top: '450px', left: '146px', width: '64px', height: '64px'}}>
                    <div className={classes.img}>
                        <Link to="/who-we-are">
                            <img alt={"shape"} src={whoWeAre} className={classes.image}/>
                        </Link>
                    </div>
                </div>
                <div style={{position: 'absolute', top: '450px', left: '466px', width: '64px', height: '64px'}}>
                    <div className={classes.img}>
                        <Link to="/mobile-applications">
                            <img alt={"shape"} src={whatWeCanDo} className={classes.image}/>
                        </Link>
                    </div>
                </div>
                <div style={{position: 'absolute', top: '450px', left: '784px', width: '64px', height: '64px'}}>
                    <div className={classes.img}>
                        <Link to="/contact-us">
                            <img alt={"shape"} src={callUs} className={classes.image}/>
                        </Link>
                    </div>
                </div>
                <div style={{
                    position: 'absolute',
                    top: '520px',
                    left: '82px',
                    zIndex: '86'
                }}>
                    <div className={classes.txt}>
                        <h3 style={{textAlign: 'center'}}>
                                <span style={{color: '#fff', fontWeight: '300', fontFamily: 'Lato-Light'}}>
                                    Learn Who
                                </span>
                            <br/>
                        </h3>
                        <h3 style={{textAlign: 'center'}}>
                            <span style={{color: '#fff', fontFamily: 'Lato-Light'}}>We Are</span>
                        </h3>
                    </div>
                </div>
                <div style={{
                    position: 'absolute',
                    top: '527px',
                    left: '401px',
                    zIndex: '86'
                }}>
                    <div className={classes.txt}>
                        <h3 style={{textAlign: 'center'}}>
                                <span style={{color: '#fff', fontWeight: '300', fontFamily: 'Lato-Light'}}>
                                    Learn What
                                </span>
                            <br/>
                        </h3>
                        <h3 style={{textAlign: 'center'}}>
                            <span style={{color: '#fff', fontFamily: 'Lato-Light'}}>We Can Do</span>
                        </h3>
                    </div>
                </div>
                <div style={{
                    position: 'absolute',
                    top: '527px',
                    left: '720px',
                    zIndex: '86'
                }}>
                    <div className={classes.txt}>
                        <h3 style={{textAlign: 'center'}}>
                                <span style={{color: '#fff', fontWeight: '300', fontFamily: 'Lato-Light'}}>
                                    Call Us
                                </span>
                            <br/>
                        </h3>
                        <h3 style={{textAlign: 'center'}}>
                            <span style={{
                                color: '#fff',
                                fontSize: '22px',
                                fontFamily: 'Lato-Light'
                            }}>(905) 637-2525</span>
                        </h3>
                    </div>
                </div>
                <div className="wsb-element-shape"
                     style={{
                         position: 'absolute',
                         height: '345px',
                         width: '100vw',
                         top: '730px',
                         left: '-300px',
                         zIndex: '96',
                         backgroundColor: '#a1a1a1'
                     }}>
                    <div className="wsb-shape"/>
                </div>
                <div className={classes.learn}>
                    <div className={classes.innerImage}>
                        <img src={learnMore} alt="learn-more"/>
                    </div>
                </div>
                <div style={{position: 'absolute', top: '790px', left: '336px', zIndex: '98'}}>
                    <div style={{width: '639px', height: '39px'}}>
                        <h3>
                            <span style={{color: '#fff', fontFamily: 'Lato-Light'}}/>
                            <span style={{
                                color: '#fff',
                                fontFamily: 'Lato-Light'
                            }}>We Are Here to Help You Succeed​</span>
                            <span style={{color: '#fff', fontFamily: 'Lato-Light'}}/>
                        </h3>
                    </div>
                </div>
                <div style={{position: 'absolute', top: '849px', left: '337px', zIndex: '100'}}>
                    <div style={{width: '575px', height: '72px'}}>
                        <span style={{fontFamily: 'Lato-Light', color: '#000', fontSize: '15px'}}>
                            Our suite of products utilizes the latest technologies to empower the mobile
                            worker in many areas including mobile sales, merchandizing and delivery, field
                            service, logistics and transportation, and warehouse management.
                        </span>
                    </div>
                </div>
                <div style={{position: 'absolute', top: '954px', left: '336px', zIndex: '98'}}>
                    <a href={"/who-we-are"}>
                        <Button className={classes.learnMoreBtn}>
                            <span style={{color: '#fff', fontFamily: 'Lato-Light', fontSize: '18px'}}>Learn More</span>
                        </Button>
                    </a>
                </div>
                <div className="wsb-element-shape"
                     style={{
                         position: 'absolute',
                         height: '258px',
                         width: '100vw',
                         top: '1075px',
                         left: '-300px',
                         zIndex: '96',
                         backgroundColor: '#e4e4e4'
                     }}>
                    <div className="wsb-shape"/>
                </div>
                <div style={{position: 'absolute', top: '1110px', left: '13px', zIndex: '98'}}>
                    <div style={{width: '593px', height: '49px'}}>
                        <h1 style={{color: '#888', fontFamily: 'Lato-Regular', fontSize: '38px'}}>Technology at Your
                            Fingertips</h1>
                    </div>
                </div>
                <div style={{position: 'absolute', top: '1178px', left: '15px', zIndex: '98'}}>
                    <div style={{width: '618px', height: '95px'}}>
                        <pre style={{color: '#000', fontFamily: 'Lato-Regular', fontSize: '1.2em'}}>
                            Please contact Azertech at your convenience for an initial consultation. We’ll work with
                            you one on one to determine a roadmap to success. Give us a call and together we can
                            make our mobile solutions work for you.
                            <br/>
                            <br/>
                           Call Today (905) 637-2525 x210
                        ​</pre>
                    </div>
                </div>
                <div style={{position: 'absolute', top: '1080px', left: '735px', zIndex: '98'}}>
                    <div style={{position: 'relative', height: '231px', width: '277px', overflow: 'hidden'}}>
                        <img src={call} alt="call" style={{position: 'absolute'}}/>
                    </div>
                </div>
            </div>
        </div>
    </div>
}