import React from "react";
import Logo from "../../assets/images/Logo.jpg";

export default function HeaderLogo() {
    return <div style={{width: '204px', textAlign: 'center', marginTop: '0', marginBottom: '0', lineHeight: '110px'}}>
        <img src={Logo} alt={"logo"}
             style={{
                 verticalAlign: 'middle',
                 width: '204px',
                 height: '85px',
                 border: '0'
             }}/>
    </div>
}