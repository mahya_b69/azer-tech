import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import copyright from "../../assets/images/copyright.png";


const useStyles = makeStyles(theme => ({
    footer: {
        position: 'relative',
        height: '200px',
        color: '#fff',
        backgroundColor: '#3e3e45',
        fontSize: '15px',
        fontFamily: 'Lato-Regular',
        zIndex: '200',

    },
    footerContainer: {
        position: 'absolute',
        width: '1000px',
        left: '50%',
        marginLeft: '-500px'

    },
    footerText: {
        top: '32px',
        left: '166px',
        position: 'absolute',
        zIndex: '14px'
    },
    footerImage: {
        top: '20px',
        left: '15px',
        position: 'absolute',
        zIndex: '11',
        color: '#fff'
    },
    footerImg: {
        width: '133px',
        height: '35px',
        position: 'relative',
        overflow: 'hidden'
    },
    copyright: {
        verticalAlign: 'middle',
        width: '133px',
        height: '35px'
    }

}))

export default function Footer(){
    const classes = useStyles();
    return(
        <div className={classes.footer}>
            <div className={classes.footerContainer}>
                <div className={classes.footerText}>
                    <div style={{width: '510px', height: '20px'}}>
                        <p>
                            <span style={{color: '#fff', fontFamily: 'Lato-Regular', fontSize: '15px'}}>Copyright © Azertech Inc. All rights reserved.</span>
                            <br/>
                        </p>
                    </div>
                </div>
                <div className={classes.footerImage}>
                    <div className={classes.footerImg}>
                        <img src={copyright} className={classes.copyright} alt={"copyright"}/>
                    </div>
                </div>
            </div>
        </div>
    )
}