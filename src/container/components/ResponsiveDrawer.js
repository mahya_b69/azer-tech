import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {makeStyles} from '@material-ui/core/styles';
import {
    Link
} from "react-router-dom";
import HeaderLogo from "./HeaderLogo";
import "../App/index.css";
import Container from "@material-ui/core/Container";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    container: {
        [theme.breakpoints.down('xs')]: {
            width: '100%',
            height: '50px',
            padding: '0',
            position: 'relative',
            top: '0',
            left: '0',
            right: '0'
        }
    },
    title: {
        width: '210px',
        boxSizing: 'border-box',
        padding: '0 15px',
        textAlign: 'center',
        position: 'absolute',
        backgroundColor: 'transparent',
        fontFamily: 'Lato-Regular',
        fontSize: '20px',
        fontWeight: '400',
        left: ' 50px',
        height: '50px',
        lineHeight: '50px',
        right: '100px',
        color: '#fff',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
            backgroundColor: 'white',
            boxShadow: '0 0 transparent',
            textAlign: 'center',
            margin: '0 auto',

        },
        [theme.breakpoints.down('xs')]: {
            width: '100vw',
            height: '50px',
            lineHeight: '50px',
            backgroundColor: '#dd492f',
            boxShadow: '0 0 transparent',

        }
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: 0,
            flexShrink: 0,
        },
    },
    menuButton1: {
        width: '48px',
        border: '0',
        left: '2px',
        right: '210px',
        backgroundColor: 'transparent',
        position: 'absolute',
        padding: '0px',
        outline: '0',
        boxSizing: 'border-box',
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    menuButton2: {
        width: '48px',
        border: '0',
        backgroundColor: 'transparent',
        position: 'absolute',
        paddingRight: '2px',
        outline: '0',
        right: '54px',
        left: '258px',
        boxSizing: 'border-box',
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    menuButton3: {
        width: '48px',
        border: '0',
        backgroundColor: 'transparent',
        position: 'absolute',
        right: '2px',
        left: '310px',
        paddingLeft: '2px',
        outline: '0',
        boxSizing: 'border-box',
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    // necessary for content to be below app bar

    toolbar: {
        [theme.breakpoints.up('sm')]: {
            width: '1000px',
            margin: '0 auto',
            backgroundColor: '#fff',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
        [theme.breakpoints.down('xs')]: {
            width: '100%',
            height: '50px',
            backgroundColor: 'transparent',
            position: 'absolute',
            top: '0',
            left: '0',
            right: '0'

        },

    },
    drawerPaper: {
        width: drawerWidth,
    },
    navBar: {
        [theme.breakpoints.up('sm')]: {
            width: '700px',
            height: '110px',
        }
    },
    list: {
        [theme.breakpoints.up('sm')]: {
            width: 'auto',
            height: '100%',
            display: 'table',
            position: 'relative',
        }
    },
    listItem: {
        [theme.breakpoints.up('sm')]: {
            margin: '0 auto',
            width: 'auto',
            height: '100%',
            textAlign: 'center',
            display: 'inline-block',

            "&:hover": {
                "& a": {
                    color: '#dd492f'
                }
            }
        }
    },
    item: {
        padding: '0 !important',
        margin: '0 !important'
    },
    listItemText: {
        [theme.breakpoints.up('sm')]: {
            color: '#000',
            padding: '42px 16.9px',
            fontWeight: '400',
            fontFamily: 'Lato',
            lineHeight: '22px',
            fontSize: '17px',
        }

    }
}));

function ResponsiveDrawer(props) {
    const {window} = props;
    const classes = useStyles();
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    const menuItems = [
        {text: 'Home', link: '/home'},
        {text: 'Who We Are', link: '/who-we-are'},
        {text: 'Mobile Application', link: '/mobile-applications'},
        {text: 'Partners', link: '/partners'},
        {text: 'Careers', link: '/careers'},
        {text: 'Contact Us', link: 'contact-us'}];

    const drawer = (
        <div>
            <List>
                {menuItems.map((item, index) => (
                    <Link to={item.link} key={index}>
                        <ListItem button key={index}>
                            <ListItemText primary={item.text}/>
                        </ListItem>
                    </Link>
                ))}
            </List>
        </div>
    );

    const container = window !== undefined ? () => window().document.body : undefined;
    return (
        <>
            <div className={classes.container}>
                <div className={classes.appBar}>
                    <div className={`${classes.toolbar} mobile-menu`}>
                        <button
                            onClick={handleDrawerToggle}
                            className={classes.menuButton1}
                        >
                            <i className="icon-wsb-mobile mobile-hamburger"/>
                        </button>
                        <span className={classes.title}>Home</span>
                        <button className={classes.menuButton2}>
                            <i className="icon-wsb-mobile phone"/>
                        </button>
                        <button className={classes.menuButton3}>
                            <i className="icon-wsb-mobile marker"/>
                        </button>

                    </div>
                </div>
                <nav className={classes.drawer} aria-label="mailbox folders">
                    <Hidden smUp implementation="css">
                        <Drawer
                            container={container}
                            variant="temporary"
                            anchor={'left'}
                            open={mobileOpen}
                            onClose={handleDrawerToggle}
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            ModalProps={{
                                keepMounted: true, // Better open performance on mobile.
                            }}
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                </nav>
            </div>
            <Hidden xsDown implementation={"css"}>
                <div className={classes.container}>
                    <div  className={classes.appBar}>
                        <div className={classes.toolbar}>
                            <HeaderLogo/>
                            <nav className={classes.navBar}>
                                <List className={classes.list} classes={{root: classes.item}}>
                                    {menuItems.map((item, index) => (
                                        <ListItem key={index} className={classes.listItem}
                                                  hover={"true"}
                                                  classes={{root: classes.item}}>
                                            <Link to={item.link} style={{display: 'block'}}
                                                  className={classes.listItemText}>
                                        <span style={{fontFamily: 'Lato-Regular'}}>
                                            {item.text}
                                        </span>
                                            </Link>
                                        </ListItem>
                                    ))}
                                </List>
                            </nav>
                        </div>
                    </div>
                </div>
            </Hidden>
        </>

    );
}

ResponsiveDrawer.propTypes = {
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window: PropTypes.func,
};

export default ResponsiveDrawer;
